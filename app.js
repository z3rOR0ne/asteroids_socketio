let socket = io()

console.log('hi from app.js')

const startingSection = document.querySelector('.starting-section')
const homeBtn = document.querySelector('.home-btn')
const startButton = document.getElementById('startButton')
let crazyButton = document.getElementById('crazyButton')

// establish connection to server
socket.on('connection', (socket) => {
    socket.on('disconnect', () => {
    })
})

// upon receipt of a socket event called hello, log the string within the hello event to the console
socket.on('hello', (msg) => {
    console.log(msg)
})

// add an event listener to our start button
startButton.addEventListener('click', () => {
    // which will send back a socket event called 'startGame' 
    socket.emit('startGame', 'startGame from app.js')
})

// upon receipt of a socket event called startGame, the hideStartButton() function is called
socket.on('startGame', () =>
    hideStartButton()
)

homeBtn.addEventListener('click', () => {
    socket.emit('returnHome', 'returnHome from app.js')
})

socket.on('returnHome', (msg) => {
    console.log(msg)
    showStartButton()
})

crazyButton.addEventListener('click', () => {
    socket.emit('crazyIsClicked', {
        offsetLeft: Math.random() * ((window.innerWidth - crazyButton.clientWidth - 100)),
        offsetTop: Math.random() * ((window.innerWidth - crazyButton.clientHeight - 50))
    })
})

socket.on('crazyIsClicked', (data) => {
    goCrazy(data.offsetLeft, data.offsetTop)
})

function hideStartButton() {
    startButton.style.display = 'none'
    crazyButton.style.display = 'block'
    startingSection.style.display = 'none'
}

function showStartButton() {
    startButton.style.display = 'block'
    crazyButton.style.display = 'none'
    startingSection.style.display = 'block'

}

function goCrazy(offLeft, offTop) {
    let top, left

    left = offLeft
    top = offTop

    crazyButton.style.top = top + 'px'
    crazyButton.style.left = left + 'px'
    crazyButton.style.animation = 'none'

}

const path = require('path')
const http = require('http')
const express = require('express')
const socketIO = require('socket.io')
const port = process.env.PORT || 3000
const publicPath = path.join(__dirname, '/../frontend')
let app = express()
let server = http.createServer(app)
let io = socketIO(server)

io.on('connection', (socket) => {

    //socket.leave(socket.id)
    //socket.join('asteroids')
    //console.log(socket.rooms)

    // display connection confirmation
    console.log(`A user just connected on socket id: ${socket.id}.`)

    socket.emit('init', socket.id)

    let currentKeyDownCode, currentKeyUpCode

    socket.on('clientKeyDown', (keyCode) => {
        currentKeyDownCode = keyCode
        socket.emit('returnKeyDown', currentKeyDownCode)
    })

    socket.on('clientKeyUp', (keyCode) => {
        currentKeyUpCode = keyCode
        socket.emit('returnKeyUp', currentKeyUpCode)
    })

    socket.on('disconnect', () => {
        console.log(`A user has disconnected from socket id: ${socket.id}`)
    })

})

app.use(express.static(publicPath))

server.listen(port, () => {
    console.log(`Server is up on port ${port}.`)
})


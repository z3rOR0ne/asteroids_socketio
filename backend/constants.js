const CANV_WIDTH = 700 // canvas width
const CANV_HEIGHT = 500 // canvas height
const FPS = 30 // frames per second
const FRICTION = 0.7 // friction coefficient of space (0 = no friction, 1 = lots of friction)
const SHIP_SIZE = 30 // ship height in pixels
const SHIP_THRUST = 5 // acceleration of the ship in pixels per second per second
const TURN_SPEED = 360 // turn speed in degrees per second

module.exports = {
    CANV_WIDTH,
    CANV_HEIGHT,
    FPS,
    FRICTION,
    SHIP_SIZE,
    SHIP_THRUST,
    TURN_SPEED
}

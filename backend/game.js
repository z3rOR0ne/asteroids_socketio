const {CANV_WIDTH, CANV_HEIGHT, SHIP_SIZE} = require('./constants')

function ship() {
    return {
        x: CANV_WIDTH / 2,
        y: CANV_HEIGHT / 2,
        r: SHIP_SIZE / 2,
        a: (90 / 180) * Math.PI, // convert to radians
        rot: 0,
        thrusting: false,
        thrust: {
            x: 0,
            y: 0,
        },
    }
}

module.exports = ship
